const Task = require("../models/Task");


module.exports.newTask = (reqBody) => {
    let addTask = new Task ({
        name: reqBody.name,
        status: reqBody.status
    })
    return addTask.save().then((task, error) => {
        if(error){
            return false;
        } else {
            return true;
        }
    })
} 

module.exports.allTask = () => {
    return Task.find({}).then(result => {
        return result;
    })
}