const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//register new user
module.exports.registerUser = (reqBody) => {
    return User.findOne({ email: reqBody.email}).then(result => {
        if(result){
            return 'Email already exist'
        } else {
            let newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 12)
            })
            return newUser.save().then((user, error) => {
                if(error){
                    return false;
                } else {
                    return 'User registration successful';
                }
            })
        }
    })  
}   

//Login a user and return a jwt token
module.exports.loginUser = (reqBody) => {
    return User.findOne ({ email: reqBody.email }).then(result => {
        if(result == null){
            return false;
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if(isPasswordCorrect){
                return { accessToken : auth.createAccessToken(result.toObject())}
            } else {
                return false;
            } 
        }
    })
}

//Retrieve all users (without password)
module.exports.getAllUsers = () => {
    return User.find({}).select('-password').then(result => {
        return result;
    })
}

//Retrieve specific user (without password)
module.exports.getProfile = (data) => {
    return User.findById(data).select('-password').then(result => {
        return result;
    })
}

//get verified data
module.exports.getData = (data) => {
    return User.findById(data).then(result => {
        return result;
    })
}