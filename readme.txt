// Install on terminal the dependencies //
- npm init
- npm install express
- npm install mongoose
- npm install dotenv
- npm install cors
- npm install bcrypt
create a path "start" for nodemon in package.json


//Create folders and files//
- touch index.js
- touch .env
- touch .gitignore
- touch auth.js
- mkdir models
    - touch Task.js
    - touch User.js
- mkdir routes
    - touch userRoutes.js
-mkdir controllers
    - touch userControllers.js


//.gitignore //
input node_modules and .env


//.env //
input PORT=4000 and the mongoDB DB_CONNECTION + your mongoDB Atlas account

//index.js
create the server 
- require dependencies
- create access to the backend
- connect to mongoDB
- connect to PORT

//routes folder
    //userRoutes.js
    - require express
    - create a route
    - export route

    //TaskRoutes.js
    - require express
    - create a route
    - export route

//controllers folder
    //userControllers.js
    - require dependencies
    - export route

    //TaskControllers.js
    - require dependencies
    - export route

//models folder
    //Task.js
    - create a variable mongoose that will require "mongoose"
    - create taskSchema
    - export taskSchema

    //User.js
    - create a variable mongoose that will require "mongoose"
    - create userSchema
    - export userSchema

//auth.js
-create a token
-create a token verification
-create a token decryption

