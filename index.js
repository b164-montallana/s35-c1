const express = require("express");
const mongoose = require("mongoose");
require('dotenv').config();
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const taskRoutes = require("./routes/taskRoutes");


const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended:true }));

app.use("/user", userRoutes)
app.use("/task", taskRoutes);

mongoose.connect(process.env.DB_CONNECTION, {
    useNewUrlParser: true,
    UseUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log('We are now connected to mongoDB Atlas'));

app.listen(process.env.PORT, () => {
    console.log(`task in now online on port ${process.env.PORT}`);
})