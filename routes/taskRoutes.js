const express = require("express");
const router = express.Router();

const TaskController = require("../controllers/taskControllers");
//create new to do list
router.post("/new", (req, res) => {
    TaskController.newTask(req.body).then(result => res.send(result));
})

//retrieve all task
router.get("/all", (req, res) => {
    TaskController.allTask(req.body).then(result => res.send(result));
})

module.exports = router;