const express = require("express");
const router = express.Router();
const auth = require("../auth")

const UserController = require("../controllers/userControllers");


//register a user
//http://localhost:4000/user/register
router.post("/register", (req, res) => {
    UserController.registerUser(req.body).then(result => res.send(result));
})

//login a user
router.post("/login", (req, res) => {
    UserController.loginUser(req.body).then(result => res.send(result));
})

//Retrieve all users (without password)
router.get("/all", (req, res) => {
    UserController.getAllUsers(req.body.id).then(result => res.send(result));
})

//Retrieve specific users (without password)
router.get("/details/:id", (req, res) => {
    UserController.getProfile(req.params.id).then(result => res.send(result));
})

//verify user 
router.get("/details", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    UserController.getData(userData.id).then(result => res.send(result))
})



module.exports = router;